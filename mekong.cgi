#!/usr/bin/perl -w
# written by andrewt@cse.unsw.edu.au October 2013
# as a starting point for COMP2041 assignment 2
# http://www.cse.unsw.edu.au/~cs2041/assignments/mekong/

use CGI qw/:all/;
use Time::localtime;

$debug = 0;
$| = 1;

if (!@ARGV) {
	# run as a CGI script
	cgi_main();
	
} else {
	# for debugging purposes run from the command line
	console_main();
}
exit 0;

# This is very simple CGI code that goes straight
# to the search screen and it doesn't format the
# search results at all

sub cgi_main {
	print page_header();
	
	set_global_variables();
	read_books($books_file);

	$login = param('login');

	my $search_terms = param('search_terms');
	my $password = param('password');
	my $register = param('register');
	my $create = param('create');
	
	    my $full_name = param('full_name');
	    my $street = param('street');
	    my $city = param('city');
	    my $state = param('state');
	    my $postcode = param('postcode');
	    my $email = param('email');
	    
	my $search= param('search');
	my $basket = param('basket');
	my $checkout = param('checkout');
	my $finalize = param('finalize');
	
	    my $credit_card_number = param('card_number');
	    my $expiry_date = param('expiry_date');
	    
	my $view_orders = param('view_orders');
	my $add = param('add');
	my $details = param('details');

	    my $isbn = param('isbn');
	    
	my $drop = param('drop');
	my $confirmation = param('confirmation');
	my $forgot_pass = param('forgot_pass');
	
	    my $email_forgot_pass = param('email_forgot_pass');
	    my $reset_pass = param('reset_pass');
	    my $new_pass = param('new_pass');
	    
	my $logout = param('logout');
	
	
	if(defined $logout){
        Delete_all();
	    print login_form();
	}
	elsif (defined $search) {
		print search_form();
	}
	elsif (defined $basket) {
		print basket_form($login);
	}
	elsif (defined $checkout) {
	    my @basket_isbns = read_basket($login);
	    if (!@basket_isbns) {
            print basket_form($login);
        }
        else{
		    print checkout_form($login);
		}
	}
	elsif (defined $finalize ){
	    if(legal_credit_card_number($credit_card_number) && legal_expiry_date($expiry_date)){
	        finalize_order($login, $credit_card_number, $expiry_date);
	        print font({-color=>'green'}, "<center>Successfully CHECKED OUT !</center>");
    		print basket_form($login);
	    }
	    else{
	        print font({-color=>'red'}, "<center>$last_error</center>");
	        print checkout_form($login);
	    }
	}
	
	elsif (defined $view_orders){
	    my @order_numbers = login_to_orders($login);
	    if (!@order_numbers) {
	        print font({-color=>'red'}, "<center>You have not made any orders</center>");
            print basket_form($login);
        }
        else{
		    print view_orders_form($login);
		}
	}
	elsif (defined $add){
	    add_basket($login,$isbn);
	    print "<center><font color='green'>Your book has been ADDED!</font><center>";
	    print basket_form($login);
	}
	elsif (defined $details){
	    print details_form($isbn);
	}
	elsif (defined $drop){
	    delete_basket($login,$isbn);
	    print "<center><font color='green'>Your book has been DELETED!</font><center>";
	    print basket_form($login);
	}
	elsif (defined $search_terms) {
		print search_results($search_terms);
	}
	elsif(defined $register){
	    print register_form();
	}
	elsif(defined $create){
        if(legal_login($login) && legal_password($password) && legal_register($login)){
            open F, ">users/$login";
            print F "password=$password\n";
            print F "name=$full_name\n";
            print F "street=$street\n";
            print F "city=$city\n";
            print F "state=$state\n";
            print F "postcode=$postcode\n";
            print F "email=$email";
            close(F);
            $body = "Please confirm you email address by the following link :
                     http://cgi.cse.unsw.edu.au/~agos568/ass2/mekong.cgi?confirmation=$login";
        	send_email($email,'agos568','mekong.com.au Email Confirmation',$body);
        	print font({-color=>'green'}, "<center>Your account has been CREATED!<br>");
	        print font({-color=>'red'}, "<br>We have sent an email verification to $email<br>");
	        print "<a href='http://cgi.cse.unsw.edu.au/~agos568/ass2/mekong.cgi'>LOGIN</a>";
	    }
	    else{
	        print font({-color=>'red'}, "<center>$last_error</center>");
	        print register_form();
	    }
	}
	elsif (defined $confirmation) {
		open F, ">>users/EMAIL_CONFIRMATION";
		print F "$confirmation\n";
		close(F);
		print font({-color=>'green'}, "<center>Thank you ! Your email has been verified <br>");
		print "<a href='http://cgi.cse.unsw.edu.au/~agos568/ass2/mekong.cgi'>LOGIN</a>";
		
	}
	elsif(defined $forgot_pass){
		print forgot_pass_form();
	}
	elsif(defined $email_forgot_pass){
	    open U, "users/$login";
	    while ($line = <U>){
	        if($line =~ /email/){
	            $line =~ s/[^\s]*=//;
	            $email = $line;
	        }      
	    }
	    close(U);
		$body = "Hi $login, Please click this link in order to reset your password :
		         http://cgi.cse.unsw.edu.au/~agos568/ass2/mekong.cgi?login=$login&reset_pass=$login";
        send_email('ndrew.g@live.com','agos568','mekong.com.au Reset Password',$body);
	    print font({-color=>'green'}, "<center><br>We have sent a link to $email<br>");
	    print "<a href='http://cgi.cse.unsw.edu.au/~agos568/ass2/mekong.cgi'>LOGIN</a>";
	}
	elsif(defined $reset_pass){
	    if(defined $new_pass){
	        open U, "<users/$login";
	        @temp = <U>;
	        close(U);
	        
	        open U, ">users/$login";
	        foreach $line (@temp){
	            if($line =~ /password/){
	                $line =~ s/=[^\s]*/=$new_pass/;
	                
	            }
	            print U $line;
	        }
	        close(U);
	        print font({-color=>'green'}, "<center>Your password has been changed !<br>");
		    print "<a href='http://cgi.cse.unsw.edu.au/~agos568/ass2/mekong.cgi'>LOGIN</a>";
	    }
	    else{
	        print "<form>
	               <center>Please enter your new pass :
	               <br>
	               <table align='center'>
		               <tr><td>New Password:
		                   <td><input type='text' name='new_pass' size=16></input>
		               <tr><td colspan='2'><input type='submit' name='reset_pass' value='Reset Password'>
	               </table>
                   <input type='hidden' name='login' value='$login'>
	               </form>
	        ";
	    }
	}
	elsif (defined $login && defined $password && authenticate($login, $password)) {
		print font({-color=>'green'}, "<center>WELCOME BACK, $login !<br>");
		if(check_email_confirmation($login)){
			print search_form();
		}
		else{
			print font({-color=>'red'}, "<br>SORRY, your email has not been verified, please check your email<br>");
			print "<a href='http://cgi.cse.unsw.edu.au/~agos568/ass2/mekong.cgi'>LOGOUT</a>";
			
		}
	}
	else {
	    print font({-color=>'red'}, "<center>$last_error</center>");
		print login_form();		

	}
	
	#print page_trailer();
}

sub send_email{
	use Mail::Mailer;
	use strict;
	my ($to, $from, $subject, $body) = ( @_);
	my $mailer = Mail::Mailer->new("sendmail");
	$mailer->open({
		From    => $from,
		To      => $to,
		Subject => $subject,
	});
	print $mailer $body;
	$mailer->close();
}

sub check_email_confirmation{
	my ($login) = @_;
	open F, "users/EMAIL_CONFIRMATION";
	while($line = <F>){
		if($line =~ /$login/){
			close(F);
			return 1;
		}
	}
	close(F);
	return 0;
}

# simple login form without authentication	
sub login_form {
	return <<eof;
	<p>
	
	<form>
	<table align="center">
		<tr><td>Login:
		    <td><input type="text" name="login" size=16 placeholder='Username'></input>
		    <td rowspan="2"><input type="submit" style="height: 70px; width: 70px" name="login" value="Login">
		<tr><td>Password:
		    <td><input type="password" name="password" size=16 placeholder='Password'></input>
	</table>
	<br>
	<table align="center">
		<tr><td width='200px'><input type="submit" name="register" value="Create New Account">
			<td width='200px'><input type="submit" name="forgot_pass" value="Forgot your password\?">
	</table>
	
	</form>
	<p>
eof
}


sub register_form {
	return <<eof;
	<p>
	<center>--------- Create a new account ----------</center><br>
	<form>
	<table align="center">
		<tr><td>Login:
		    <td><input type="text" name="login" size=16></input>
		<tr><td>Password:
		    <td><input type="password" name="password" size=16></input>
		    <tr><td>Full Name:
		    <td><input type="text" name="full_name" size=16></input>
		    <tr><td>Street:
		    <td><input type="text" name="street" size=16></input>
		    <tr><td>City/Suburb:
		    <td><input type="text" name="city" size=16></input>
		    <tr><td>State:
		    <td><input type="text" name="state" size=16></input>
		    <tr><td>Postcode:
		    <td><input type="text" name="postcode" size=16></input>
		    <tr><td>Email Address:
		    <td><input type="text" name="email" size=16></input>
		    
		<tr><td colspan='2'><input class="btn" type="submit" name="create" value="Create Account">
		
	</table>
	</form>
	<p>
eof
}

sub forgot_pass_form{

	return <<eof;
	<p>
	<center>--------- Password Reset ----------</center><br>
	<form>
	<center>We will send an email to your email :<br>
	<br>
	<table align="center">
		<tr><td>Login Username:
		    <td><input type="text" name="login" size=16></input>
		<tr><td colspan="2"><input type="submit" name="email_forgot_pass" value="Reset Password">
	</table>
	
	</form>
	<p>
eof
}

# simple search form
sub search_form {

	return <<eof;
	<p>
	<form>
	<table align="center">
	<tr><td width='100px'>Search:
	    <td><input type='text' name='search_terms' size=40></input>
	    <td><input class='btn' type='submit' value='Search!'>
	</table>
    <br>
	<table align="center">
	<tr><td><input class='btn' type='submit' name='basket' value='Basket'>
	    <td><input class='btn' type='submit' name='checkout' value='Check Out'>
	    <td><input class='btn' type='submit' name='view_orders' value='View Orders'>
	    <td><input class='btn' type='submit' name='logout' value='Logout'>
	</table>
	<input type='hidden' name='login' value='$login'>
	</form>
	<p>
eof
}

# ascii display of search results
sub search_results {
	my ($search_terms) = @_;
	my @matching_isbns = search_books($search_terms);
	my $descriptions = get_book_descriptions(@matching_isbns);

    print "<p>
	<form>
	<table align='center'>
	<tr><td>Search: 
	    <td><input type='text' name='search_terms' size=60 value='$search_terms'></input>
	</table>
	<input type='hidden' name='login' value='$login'>
	</form>
	<p>";
    print "<table class='border' width='90%' align='center'>";
    foreach $isbn (@matching_isbns){
    
        print "
        <tr>
        <td><img src='$book_details{$isbn}{smallimageurl}'>
        <td><b>$book_details{$isbn}{title}</b><br>$book_details{$isbn}{authors}
        <td>$book_details{$isbn}{price}
        <form>
        <td><input class='btn' type='submit' name='add' value='Add'><br>
            <input class='btn' type='submit' name='details' value='Details'>
            <input type='hidden' name='isbn' value='$isbn'>
            <input type='hidden' name='login' value='$login'>
            <input type='hidden' name='search_terms' value='$search_terms'>

        </form>
        ";
    }

    print "</table>";
	return <<eof;
    <p>
	<form>
	<table align="center">
	<tr><td><input class='btn' type='submit' name='basket' value='Basket'>
	    <td><input class='btn' type='submit' name='checkout' value='Check Out'>
	    <td><input class='btn' type='submit' name='view_orders' value='View Orders'>
	    <td><input class='btn' type='submit' name='logout' value='Logout'>
	</table>
	<input type='hidden' name='login' value='$login'>
	</form>
	<p>
eof
}

sub basket_form {
    my ($login) = @_;
    
    print "<center>--------- Your Shopping Basket ----------</center><br>";
    print "<table class='border' width='90%' align='center'>";

    my $total_price = 0;
    my @basket_isbns = read_basket($login);
    
	foreach $isbn (@basket_isbns){
	    chomp $isbn;
	    $book_price = $book_details{$isbn}{price};
	    $book_price =~ s/\$//;
	    $total_price += $book_price;
	    print "
        <tr>
        <td><img src='$book_details{$isbn}{smallimageurl}'>
        <td><b>$book_details{$isbn}{title}</b><br>$book_details{$isbn}{authors}
        <td>$book_details{$isbn}{price}
        <form>
        <td><input class='btn' type='submit' name='drop' value='Drop'><br>
            <input class='btn' type='submit' name='details' value='Details'>
            <input type='hidden' name='isbn' value='$isbn'>
            <input type='hidden' name='login' value='$login'>
        </form>
        ";

	}
    if($total_price == 0) {
        print "<center>is currently EMPTY</center>";
    }
	else{
	    print "
	           <tr>
	           <td><b>Total</b>
	           <td>
	           <td><b>\$$total_price</b>
	           <td>
	           </table>
	           ";
	}
	
	return <<eof;
    <p>
	<form>
	<table align="center">
	<tr><td><input class='btn' type='submit' name='search' value='Search Books'>
	    <td><input class='btn' type='submit' name='checkout' value='Check Out'>
	    <td><input class='btn' type='submit' name='view_orders' value='View Orders'>
	    <td><input class='btn' type='submit' name='logout' value='Logout'>
	</table>
	<input type='hidden' name='login' value='$login'>
	</form>
	<p>
eof
}

sub checkout_form {
    my ($login) = @_;
    
    print "<center>---------- Checkout Form ----------</center><br>";
    print "<table class='border' width='90%' align='center'>";

    my $total_price = 0;
    my @basket_isbns = read_basket($login);
    
	foreach $isbn (@basket_isbns){
	    chomp $isbn;
	    $book_price = $book_details{$isbn}{price};
	    $book_price =~ s/\$//;
	    $total_price += $book_price;
	    print "
        <tr>
        <td><img src='$book_details{$isbn}{smallimageurl}'>
        <td><b>$book_details{$isbn}{title}</b><br>$book_details{$isbn}{authors}
        <td>$book_details{$isbn}{price}

        ";

	}

	print "
	       <tr>
	       <td><b>Total</b>
	       <td>
	       <td><b>\$$total_price</b>
	       <td>
	       </table>
	       <br>
	       <center><b>Shipping Details:</b><br><br>
	       
	       ";
	open U, "users/$login";
	while ($line = <U>){
	    chomp $line;
	    if($line =~ /password/){
	        next;
	    }
	    $line =~ s/^.*=//;
	    print "$line<br>";
	}

	close(U);
	return <<eof;
	
    <br>
	<form>
	<table align='center'>
	<tr><td>Credit Card Number:
	    <td><input type='text' name='card_number' size=14 placeholder='0000000000000000'>
	    <td rowspan='2'><input class='btn' type='submit' style="height: 70px; width: 70px" name='finalize' value="Finalize &#10; Order ">
	<tr><td>Expiry Date (mm/yy):
	    <td><input type='text' name='expiry_date' size=3 placeholder='MM/YY'>
	</table>
	<table align="center">
	<tr><td><input class='btn' type='submit' name='search' value='Search Books'>
	    <td><input class='btn' type='submit' name='basket' value='Basket'>
	    
	    <td><input class='btn' type='submit' name='view_orders' value='View Orders'>
	    <td><input class='btn' type='submit' name='logout' value='Logout'>
	</table>
	<input type='hidden' name='login' value='$login'>
	</form>
	
eof
}

sub view_orders_form {
    my ($login) = @_;
     
    print "<center><h4>----------- Your Orders ----------</h4></center><br>";
    @orders = login_to_orders($login);

    foreach $order (@orders){
        print "<center>Order #$order - ";
        @order_details = read_order($order);
             
        foreach $detail (@order_details){
            chomp $detail;
            if($detail =~ /1[0-9]{9}/ && $detail == $order_details[0]){
                $time = $detail;
            }
            elsif($detail =~ /[0-9]{16}/){
                $credit_card_number = $detail;
            }
            elsif($detail =~ /[\d]{2}\/[\d]{2}/){
                $expiry_date = $detail;
            }
            elsif($detail =~ /[0-9]{10}/){
                push(@books,"$detail");
            }
        }  
    
        print ctime($time), "<br>";
        print "Credit Card Number: $credit_card_number<br>";
        print "Expiry ($expiry_date)<br></center>";
        print "<table class='border' width='90%' align='center'>";
        
        foreach $isbn (@books){
	        chomp $isbn;
            $book_price = $book_details{$isbn}{price};
	        $book_price =~ s/\$//;
	        $total_price += $book_price;
	        print "
            <tr>
            <td><img src='$book_details{$isbn}{smallimageurl}'>
            <td><b>$book_details{$isbn}{title}</b><br>$book_details{$isbn}{authors}
            <td>$book_details{$isbn}{price}

            ";

	    }

        print "
           <tr>
	       <td><b>Total</b>
	       <td>
	       <td><b>\$$total_price</b>
	       <td>
	       </table>
           <br><br>";
        
        undef @books;
    }
    
	return <<eof;
    <table align="center">
    <form>
	<tr><td><input class='btn' type='submit' name='search' value='Search Books'>
	    <td><input class='btn' type='submit' name='basket' value='Basket'>
	    <td><input class='btn' type='submit' name='checkout' value='Check Out'>
	    <td><input class='btn' type='submit' name='logout' value='Logout'>
	    <input type='hidden' name='login' value='$login'>
	</form>
	</table>
eof
}

sub details_form {
    my ($isbn) = @_;
    print"
    <center><h3>$book_details{$isbn}{title}</h3></center>
    $book_details{$isbn}{productdescription}
    <center><img src='$book_details{$isbn}{largeimageurl}'>
    <table class='border' align='center'>
    <form>";
    
    %details = (
    "title"  => "Title",
    "authors"  => "Authors",
    "year"    => "Year",
    "isbn"    => "ISBN",
    "ean"    => "EAN",
    "numpages"  => "Number of pages",
    "price"    => "Price",
    "binding"    => "Binding",
    "catalog"  => "catalog",
    "edition"  => "Edition",
    "publication_date"  => "Publication Date",
    "publisher"    => "Publisher",
    "releasedate"  => "Release Date",
    "salesrank"    => "Sales Rank"
    );
    foreach $detail (sort keys %details){
        if(defined($book_details{$isbn}{$detail})){
            print"<tr><td><b>$details{$detail}</b>
	              <td>$book_details{$isbn}{$detail}";
        }
        
    }
   
	return <<eof;
    
	
	</form>
	</table>
	
	<table align="center">
    <form>
	<tr><td><input class='btn' type='submit' name='add' value='Add'>
	        <input type='hidden' name='login' value='$login'>
	        <input type='hidden' name='isbn' value='$isbn'>
	    <td><input class='btn' type='submit' name='basket' value='Basket'>
	    <td><input class='btn' type='submit' name='checkout' value='Check Out'>
	    <td><input class='btn' type='submit' name='logout' value='Logout'>
	</form>
	</table>
eof
}

#
# HTML at top of every screen
#
sub page_header() {
	return <<eof;
Content-Type: text/html

<!DOCTYPE html>
<html lang="en">
<head>
<title>mekong.com.au</title>
<link rel="stylesheet" type="text/css" href="css.css" >
</head>
<body>
<center> <h1>MEKONG.COM.AU</h1></font></center>

<hr>
<p>
<div class="container">
eof
}

#
# HTML at bottom of every screen
#
sub page_trailer() {
	my $debugging_info = debugging_info();
	
	return <<eof;
	$debugging_info
	</div>
<body>
</html>
eof
}

#
# Print out information for debugging purposes
#
sub debugging_info() {
	my $params = "";
	foreach $p (param()) {
		$params .= "param($p)=".param($p)."\n"
	}

	return <<eof;
<hr>
<h4>Debugging information - parameter values supplied to $0</h4>
<pre>
$params
</pre>
<hr>
eof
}




###
### Below here are utility functions
### Most are unused by the code above, but you will 
### need to use these functions (or write your own equivalent functions)
### 
###

# return true if specified string can be used as a login

sub legal_login {
	my ($login) = @_;
	our ($last_error);

	if ($login !~ /^[a-zA-Z][a-zA-Z0-9]*$/) {
		$last_error = "Invalid login '$login': logins must start with a letter and contain only letters and digits.";
		return 0;
	}
	if (length $login < 3 || length $login > 8) {
		$last_error = "Invalid login: logins must be 3-8 characters long.";
		return 0;
	}
	return 1;
}


# return true if specified string can be used as a password

sub legal_password {
	my ($password) = @_;
	our ($last_error);
	
	if ($password =~ /\s/) {
		$last_error = "Invalid password: password can not contain white space.";
		return 0;
	}
	if (length $password < 5) {
		$last_error = "Invalid password: passwords must contain at least 5 characters.";
		return 0;
	}
	return 1;
}

sub legal_register {
	my ($login) = @_;
	our ($last_error);
    if (-e "users/$login") {
        $last_error = "Invalid user name: login already exists.";
        return 0;
    }
	
	return 1;
}

# return true if specified string could be an ISBN

sub legal_isbn {
	my ($isbn) = @_;
	our ($last_error);
	
	return 1 if $isbn =~ /^\d{9}(\d|X)$/;
	$last_error = "Invalid isbn '$isbn' : an isbn must be exactly 10 digits.";
	return 0;
}


# return true if specified string could be an credit card number

sub legal_credit_card_number {
	my ($number) = @_;
	our ($last_error);
	
	return 1 if $number =~ /^\d{16}$/;
	$last_error = "Invalid credit card number - must be 16 digits.\n";
	return 0;
}

# return true if specified string could be an credit card expiry date

sub legal_expiry_date {
	my ($expiry_date) = @_;
	our ($last_error);
	
	return 1 if $expiry_date =~ /^\d\d\/\d\d$/;
	$last_error = "Invalid expiry date - must be mm/yy, e.g. 11/04.\n";
	return 0;
}



# return total cost of specified books

sub total_books {
	my @isbns = @_;
	our %book_details;
	$total = 0;
	foreach $isbn (@isbns) {
		die "Internal error: unknown isbn $isbn  in total_books" if !$book_details{$isbn}; # shouldn't happen
		my $price = $book_details{$isbn}{price};
		$price =~ s/[^0-9\.]//g;
		$total += $price;
	}
	return $total;
}

# return true if specified login & password are correct
# user's details are stored in hash user_details

sub authenticate {
	my ($login, $password) = @_;
	our (%user_details, $last_error);
	
	return 0 if !legal_login($login);
	if (!open(USER, "$users_dir/$login")) {
		$last_error = "User '$login' does not exist.";
		return 0;
	}
	my %details =();
	while (<USER>) {
		next if !/^([^=]+)=(.*)/;
		$details{$1} = $2;
	}
	close(USER);
	foreach $field (qw(name street city state postcode password)) {
		if (!defined $details{$field}) {
 	 	 	$last_error = "Incomplete user file: field $field missing";
			return 0;
		}
	}
	if ($details{"password"} ne $password) {
  	 	$last_error = "Incorrect password.";
		return 0;
	 }
	 %user_details = %details;
  	 return 1;
}

# read contents of files in the books dir into the hash book
# a list of field names in the order specified in the file
 
sub read_books {
	my ($books_file) = @_;
	our %book_details;
	print STDERR "read_books($books_file)\n" if $debug;
	open BOOKS, $books_file or die "Can not open books file '$books_file'\n";
	my $isbn;
	while (<BOOKS>) {
		if (/^\s*"(\d+X?)"\s*:\s*{\s*$/) {
			$isbn = $1;
			next;
		}
		next if !$isbn;
		my ($field, $value);
		if (($field, $value) = /^\s*"([^"]+)"\s*:\s*"(.*)",?\s*$/) {
			$attribute_names{$field}++;
			print STDERR "$isbn $field-> $value\n" if $debug > 1;
			$value =~ s/([^\\]|^)\\"/$1"/g;
	  		$book_details{$isbn}{$field} = $value;
		} elsif (($field) = /^\s*"([^"]+)"\s*:\s*\[\s*$/) {
			$attribute_names{$1}++;
			my @a = ();
			while (<BOOKS>) {
				last if /^\s*\]\s*,?\s*$/;
				push @a, $1 if /^\s*"(.*)"\s*,?\s*$/;
			}
	  		$value = join("\n", @a);
			$value =~ s/([^\\]|^)\\"/$1"/g;
	  		$book_details{$isbn}{$field} = $value;
	  		print STDERR "book{$isbn}{$field}=@a\n" if $debug > 1;
		}
	}
	close BOOKS;
}

# return books matching search string

sub search_books {
	my ($search_string) = @_;
	$search_string =~ s/\s*$//;
	$search_string =~ s/^\s*//;
	return search_books1(split /\s+/, $search_string);
}

# return books matching search terms

sub search_books1 {
	my (@search_terms) = @_;
	our %book_details;
	print STDERR "search_books1(@search_terms)\n" if $debug;
	my @unknown_fields = ();
	foreach $search_term (@search_terms) {
		push @unknown_fields, "'$1'" if $search_term =~ /([^:]+):/ && !$attribute_names{$1};
	}
	printf STDERR "$0: warning unknown field%s: @unknown_fields\n", (@unknown_fields > 1 ? 's' : '') if @unknown_fields;
	my @matches = ();
	BOOK: foreach $isbn (sort keys %book_details) {
		my $n_matches = 0;
		if (!$book_details{$isbn}{'=default_search='}) {
			$book_details{$isbn}{'=default_search='} = ($book_details{$isbn}{title} || '')."\n".($book_details{$isbn}{authors} || '');
			print STDERR "$isbn default_search -> '".$book_details{$isbn}{'=default_search='}."'\n" if $debug;
		}
		print STDERR "search_terms=@search_terms\n" if $debug > 1;
		foreach $search_term (@search_terms) {
			my $search_type = "=default_search=";
			my $term = $search_term;
			if ($search_term =~ /([^:]+):(.*)/) {
				$search_type = $1;
				$term = $2;
			}
			print STDERR "term=$term\n" if $debug > 1;
			while ($term =~ s/<([^">]*)"[^"]*"([^>]*)>/<$1 $2>/g) {}
			$term =~ s/<[^>]+>/ /g;
			next if $term !~ /\w/;
			$term =~ s/^\W+//g;
			$term =~ s/\W+$//g;
			$term =~ s/[^\w\n]+/\\b +\\b/g;
			$term =~ s/^/\\b/g;
			$term =~ s/$/\\b/g;
			next BOOK if !defined $book_details{$isbn}{$search_type};
			print STDERR "search_type=$search_type term=$term book=$book_details{$isbn}{$search_type}\n" if $debug;
			my $match;
			eval {
				my $field = $book_details{$isbn}{$search_type};
				# remove text that looks like HTML tags (not perfect)
				while ($field =~ s/<([^">]*)"[^"]*"([^>]*)>/<$1 $2>/g) {}
				$field =~ s/<[^>]+>/ /g;
				$field =~ s/[^\w\n]+/ /g;
				$match = $field !~ /$term/i;
			};
			if ($@) {
				$last_error = $@;
				$last_error =~ s/;.*//;
				return (); 
			}
			next BOOK if $match;
			$n_matches++;
		}
		push @matches, $isbn if $n_matches > 0;
	}
	
	sub bySalesRank {
		my $max_sales_rank = 100000000;
		my $s1 = $book_details{$a}{SalesRank} || $max_sales_rank;
		my $s2 = $book_details{$b}{SalesRank} || $max_sales_rank;
		return $a cmp $b if $s1 == $s2;
		return $s1 <=> $s2;
	}
	
	return sort bySalesRank @matches;
}


# return books in specified user's basket

sub read_basket {
	my ($login) = @_;
	our %book_details;
	open F, "$baskets_dir/$login" or return ();
	my @isbns = <F>;

	close(F);
	chomp(@isbns);
	!$book_details{$_} && die "Internal error: unknown isbn $_ in basket\n" foreach @isbns;
	return @isbns;
}


# delete specified book from specified user's basket
# only first occurance is deleted

sub delete_basket {
	my ($login, $delete_isbn) = @_;
	my @isbns = read_basket($login);
	open F, ">$baskets_dir/$login" or die "Can not open $baskets_dir/$login: $!";
	foreach $isbn (@isbns) {
		if ($isbn eq $delete_isbn) {
			$delete_isbn = "";
			next;
		}
		print F "$isbn\n";
	}
	close(F);
	unlink "$baskets_dir/$login" if ! -s "$baskets_dir/$login";
}


# add specified book to specified user's basket

sub add_basket {
	my ($login, $isbn) = @_;
	open F, ">>$baskets_dir/$login" or die "Can not open $baskets_dir/$login::$! \n";
	print F "$isbn\n";
	close(F);
}


# finalize specified order

sub finalize_order {
	my ($login, $credit_card_number, $expiry_date) = @_;
	my $order_number = 0;

	if (open ORDER_NUMBER, "$orders_dir/NEXT_ORDER_NUMBER") {
		$order_number = <ORDER_NUMBER>;
		chomp $order_number;
		close(ORDER_NUMBER);
	}
	$order_number++ while -r "$orders_dir/$order_number";
	open F, ">$orders_dir/NEXT_ORDER_NUMBER" or die "Can not open $orders_dir/NEXT_ORDER_NUMBER: $!\n";
	print F ($order_number + 1);
	close(F);

	my @basket_isbns = read_basket($login);
	open ORDER,">$orders_dir/$order_number" or die "Can not open $orders_dir/$order_number:$! \n";
	print ORDER "order_time=".time()."\n";
	print ORDER "credit_card_number=$credit_card_number\n";
	print ORDER "expiry_date=$expiry_date\n";
	print ORDER join("\n",@basket_isbns)."\n";
	close(ORDER);
	unlink "$baskets_dir/$login";
	
	open F, ">>$orders_dir/$login" or die "Can not open $orders_dir/$login:$! \n";
	print F "$order_number\n";
	close(F);
	
}


# return order numbers for specified login

sub login_to_orders {
	my ($login) = @_;
	open F, "$orders_dir/$login" or return ();
	@order_numbers = <F>;
	close(F);
	chomp(@order_numbers);
	return @order_numbers;
}



# return contents of specified order

sub read_order {
	my ($order_number) = @_;
	open F, "$orders_dir/$order_number" or warn "Can not open $orders_dir/$order_number:$! \n";
	@lines = <F>;
	close(F);
	chomp @lines;
	foreach (@lines[0..2]) {s/.*=//};
	return @lines;
}

###
### functions below are only for testing from the command line
### Your do not need to use these funtions
###

sub console_main {
	set_global_variables();
	$debug = 1;
	foreach $dir ($orders_dir,$baskets_dir,$users_dir) {
		if (! -d $dir) {
			print "Creating $dir\n";
			mkdir($dir, 0777) or die("Can not create $dir: $!");
		}
	}
	read_books($books_file);
	my @commands = qw(login new_account search details add drop basket checkout orders quit);
	my @commands_without_arguments = qw(basket checkout orders quit);
	my $login = "";
	
	print "mekong.com.au - ASCII interface\n";
	while (1) {
		$last_error = "";
		print "> ";
		$line = <STDIN> || last;
		$line =~ s/^\s*>\s*//;
		$line =~ /^\s*(\S+)\s*(.*)/ || next;
		($command, $argument) = ($1, $2);
		$command =~ tr/A-Z/a-z/;
		$argument = "" if !defined $argument;
		$argument =~ s/\s*$//;
		
		if (
			$command !~ /^[a-z_]+$/ ||
			!grep(/^$command$/, @commands) ||
			grep(/^$command$/, @commands_without_arguments) != ($argument eq "") ||
			($argument =~ /\s/ && $command ne "search")
		) {
			chomp $line;
			$line =~ s/\s*$//;
			$line =~ s/^\s*//;
			incorrect_command_message("$line");
			next;
		}

		if ($command eq "quit") {
			print "Thanks for shopping at mekong.com.au.\n";
			last;
		}
		if ($command eq "login") {
			$login = login_command($argument);
			next;
		} elsif ($command eq "new_account") {
			$login = new_account_command($argument);
			next;
		} elsif ($command eq "search") {
			search_command($argument);
			next;
		} elsif ($command eq "details") {
			details_command($argument);
			next;
		}
		
		if (!$login) {
			print "Not logged in.\n";
			next;
		}
		
		if ($command eq "basket") {
			basket_command($login);
		} elsif ($command eq "add") {
			add_command($login, $argument);
		} elsif ($command eq "drop") {
			drop_command($login, $argument);
		} elsif ($command eq "checkout") {
			checkout_command($login);
		} elsif ($command eq "orders") {
			orders_command($login);
		} else {
			warn "internal error: unexpected command $command";
		}
	}
}

sub login_command {
	my ($login) = @_;
	if (!legal_login($login)) {
		print "$last_error\n";
		return "";
	}
	if (!-r "$users_dir/$login") {
		print "User '$login' does not exist.\n";
		return "";
	}
	printf "Enter password: ";
	my $pass = <STDIN>;
	chomp $pass;
	if (!authenticate($login, $pass)) {
		print "$last_error\n";
		return "";
	}
	$login = $login;
	print "Welcome to mekong.com.au, $login.\n";
	return $login;
}

sub new_account_command {
	my ($login) = @_;
	if (!legal_login($login)) {
		print "$last_error\n";
		return "";
	}
	if (-r "$users_dir/$login") {
		print "Invalid user name: login already exists.\n";
		return "";
	}
	if (!open(USER, ">$users_dir/$login")) {
		print "Can not create user file $users_dir/$login: $!";
		return "";
	}
	foreach $description (@new_account_rows) {
		my ($name, $label)  = split /\|/, $description;
		next if $name eq "login";
		my $value;
		while (1) {
			print "$label ";
			$value = <STDIN>;
			exit 1 if !$value;
			chomp $value;
			if ($name eq "password" && !legal_password($value)) {
				print "$last_error\n";
				next;
			}
			last if $value =~ /\S+/;
		}
		$user_details{$name} = $value;
		print USER "$name=$value\n";
	}
	close(USER);
	print "Welcome to mekong.com.au, $login.\n";
	return $login;
}

sub search_command {
	my ($search_string) = @_;
	$search_string =~ s/\s*$//;
	$search_string =~ s/^\s*//;
	search_command1(split /\s+/, $search_string);
}

sub search_command1 {
	my (@search_terms) = @_;
	my @matching_isbns = search_books1(@search_terms);
	if ($last_error) {
		print "$last_error\n";
	} elsif (@matching_isbns) {
		print_books(@matching_isbns);
	} else {
		print "No books matched.\n";
	}
}

sub details_command {
	my ($isbn) = @_;
	our %book_details;
	if (!legal_isbn($isbn)) {
		print "$last_error\n";
		return;
	}
	if (!$book_details{$isbn}) {
		print "Unknown isbn: $isbn.\n";
		return;
	}
	print_books($isbn);
	foreach $attribute (sort keys %{$book_details{$isbn}}) {
		next if $attribute =~ /Image|=|^(|price|title|authors|productdescription)$/;
		print "$attribute: $book_details{$isbn}{$attribute}\n";
	}
	my $description = $book_details{$isbn}{productdescription} or return;
	$description =~ s/\s+/ /g;
	$description =~ s/\s*<p>\s*/\n\n/ig;
	while ($description =~ s/<([^">]*)"[^"]*"([^>]*)>/<$1 $2>/g) {}
	$description =~ s/(\s*)<[^>]+>(\s*)/$1 $2/g;
	$description =~ s/^\s*//g;
	$description =~ s/\s*$//g;
	print "$description\n";
}

sub basket_command {
	my ($login) = @_;
	my @basket_isbns = read_basket($login);
	if (!@basket_isbns) {
		print "Your shopping basket is empty.\n";
	} else {
		print_books(@basket_isbns);
		printf "Total: %11s\n", sprintf("\$%.2f", total_books(@basket_isbns));
	}
}

sub add_command {
	my ($login,$isbn) = @_;
	our %book_details;
	if (!legal_isbn($isbn)) {
		print "$last_error\n";
		return;
	}
	if (!$book_details{$isbn}) {
		print "Unknown isbn: $isbn.\n";
		return;
	}
	add_basket($login, $isbn);
}

sub drop_command {
	my ($login,$isbn) = @_;
	my @basket_isbns = read_basket($login);
	if (!legal_isbn($argument)) {
		print "$last_error\n";
		return;
	}
	if (!grep(/^$isbn$/, @basket_isbns)) {
		print "Isbn $isbn not in shopping basket.\n";
		return;
	}
	delete_basket($login, $isbn);
}

sub checkout_command {
	my ($login) = @_;
	my @basket_isbns = read_basket($login);
	if (!@basket_isbns) {
		print "Your shopping basket is empty.\n";
		return;
	}
	print "Shipping Details:\n$user_details{name}\n$user_details{street}\n$user_details{city}\n$user_details{state}, $user_details{postcode}\n\n";
	print_books(@basket_isbns);
	printf "Total: %11s\n", sprintf("\$%.2f", total_books(@basket_isbns));
	print "\n";
	my ($credit_card_number, $expiry_date);
	while (1) {
			print "Credit Card Number: ";
			$credit_card_number = <>;
			exit 1 if !$credit_card_number;
			$credit_card_number =~ s/\s//g;
			next if !$credit_card_number;
			last if $credit_card_number =~ /^\d{16}$/;
			last if legal_credit_card_number($credit_card_number);
			print "$last_error\n";
	}
	while (1) {
			print "Expiry date (mm/yy): ";
			$expiry_date = <>;
			exit 1 if !$expiry_date;
			$expiry_date =~ s/\s//g;
			next if !$expiry_date;
			last if legal_expiry_date($expiry_date);
			print "$last_error\n";
	}
	finalize_order($login, $credit_card_number, $expiry_date);
}

sub orders_command {
	my ($login) = @_;
	print "\n";
	foreach $order (login_to_orders($login)) {
		my ($order_time, $credit_card_number, $expiry_date, @isbns) = read_order($order);
		$order_time = localtime($order_time);
		print "Order #$order - $order_time\n";
		print "Credit Card Number: $credit_card_number (Expiry $expiry_date)\n";
		print_books(@isbns);
		print "\n";
	}
}

# print descriptions of specified books
sub print_books(@) {
	my @isbns = @_;
	print get_book_descriptions(@isbns);
}

# return descriptions of specified books
sub get_book_descriptions {
	my @isbns = @_;
	my $descriptions = "";
	our %book_details;
	foreach $isbn (@isbns) {
		die "Internal error: unknown isbn $isbn in print_books\n" if !$book_details{$isbn}; # shouldn't happen
		my $title = $book_details{$isbn}{title} || "";
		my $authors = $book_details{$isbn}{authors} || "";
		$authors =~ s/\n([^\n]*)$/ & $1/g;
		$authors =~ s/\n/, /g;
		$descriptions .= sprintf "%s %7s %s - %s\n", $isbn, $book_details{$isbn}{price}, $title, $authors;
	}
	return $descriptions;
}

sub set_global_variables {
	$base_dir = ".";
	$books_file = "$base_dir/books.json";
	$orders_dir = "$base_dir/orders";
	$baskets_dir = "$base_dir/baskets";
	$users_dir = "$base_dir/users";
	$last_error = "";
	%user_details = ();
	%book_details = ();
	%attribute_names = ();
	@new_account_rows = (
		  'login|Login:|10',
		  'password|Password:|10',
		  'name|Full Name:|50',
		  'street|Street:|50',
		  'city|City/Suburb:|25',
		  'state|State:|25',
		  'postcode|Postcode:|25',
		  'email|Email Address:|35'
		  );
}


sub incorrect_command_message {
	my ($command) = @_;
	print "Incorrect command: $command.\n";
	print <<eof;
Possible commands are:
login <login-name>
new_account <login-name>                    
search <words>
details <isbn>
add <isbn>
drop <isbn>
basket
checkout
orders
quit
eof
}


